package com.hbell.training.scala.spark.model

case class Match(
                  leagueId: String,
                  season: String,
                  homeTeamId: String,
                  awayTeamId: String,
                  homeTeamGoal: String,
                  awayTeamGoal: String) {


  def homePoints: Int = {
    homeTeamGoal.toInt - awayTeamGoal.toInt match {
      case a if a > 0 => 3
      case a if a < 0 => 0
      case _ => 1
    }
  }

  def awayPoints: Int = {
    awayTeamGoal.toInt - homeTeamGoal.toInt match {
      case a if a > 0 => 3
      case a if a < 0 => 0
      case _ => 1
    }
  }

}


case class MatchTeam(
                      leagueId: String,
                      season: String,
                      teamId: String,
                      points: Int = 0
                    )